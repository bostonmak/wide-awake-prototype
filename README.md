# README #

Version history:

0.1 -- basics of movement, navmeshes, character models

0.2 -- updated movement, new models for the bleak, core mechanic: attacking on the beat does more damage, death PE, beat counter (manual rhythm only)

0.3 -- movement is now more "RPG-like" but really choppy.  New terrain, but now a broken navmesh.  Animations are partially in.  Will be soon.  Lots of things cleaned up with the camera. New song.

Note: you'll here a loud "clang" when you successfully attack on the beat (and now the camera shakes!)

### How do I get set up? ###

Git clone the repository using your favorite client/terminal.  If you need help, ask Johnathan.
Open the project in Unity, and hit play!

Specific instructions for scripters, level designers, etc., forthcoming.

### Contribution guidelines ###

Please refer to https://docs.google.com/spreadsheets/d/1NIhA4Fapq3GkIMQCAvbNPtTsaFR65KatU7Q2jZGrbZc/edit#gid=0 for current tasks.

### Who do I talk to? ###

Johnathan Mell