﻿using UnityEngine;
using System.Collections;

public class CubeMover : MonoBehaviour 
{
	public float moveSpeed = 20;



	void Update ()
	{
		Vector3 newPosition = GetComponent<Transform>().position;
		if (Input.GetKeyDown(KeyCode.W))
		{
			newPosition += GetComponent<Transform>().forward * moveSpeed * Time.deltaTime;
		}
		GetComponent<Transform>().position = newPosition;
	}
}