﻿using UnityEngine;
using System.Collections;

public class AnimationTest : MonoBehaviour {

    public Animator anim;
	// Use this for initialization
	void Start () {
        if (anim) {
            Debug.Log(anim);
        }
	}
	
	// Update is called once per frame
	void Update () {
        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        Debug.Log(info.IsName("Death"));
        if (info.normalizedTime > 1) {
            if (info.IsName("Walking")) {
                anim.SetBool("IsWalking", false);
            }
            else if (info.IsName("Jumping")) {
                anim.SetBool("IsJumping", false);
            }
            else if (info.IsName("Attacking")) {
                anim.SetBool("IsAttacking", false);
            }
        }
	}
}
