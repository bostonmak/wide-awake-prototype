using UnityEngine;
using System.Collections;

public class Beat : MonoBehaviour
{
    public float timeInterval;
    public AudioClip mainBeat;
    public AudioClip halfBeat;
    public AudioClip quarterBeat;
	public AudioClip success;
    public Player player;
    private int dir;
    private int state = 0;
    //public GlowEffect glow;
    //THIS IS THE GLOW VALUE
    float x;
    public static bool paused;
    public bool menu;
    public AudioClip menuMusic;
	private float window = 0.0f;

    void Start()
    {
        InvokeRepeating("PlayBeat", 1, timeInterval);
        InvokeRepeating("HalfBeat", 1, timeInterval / 2);
        InvokeRepeating("QuarterBeat", 1, timeInterval / 4);
        HalfBeat();
        paused = false;
        Time.timeScale = 1;
        state = 0;
        dir = 1;
    }

    void Update()
    {
        if (x > .25f)
        {
            x -= Time.deltaTime;
        }
		window -= Time.deltaTime;
		if (window <= 0)
			player.onBeat = false;
        //glow.glowTint = new Color(x, x, x);
    }

	public void playSuccess()
	{
		GetComponent<AudioSource>().PlayOneShot(success, 1.0f);
	}

    //every second
    void PlayBeat()
    {
		player.onBeat = true;
		window = timeInterval/8;
        if (menu)
        {
            menu = false;
            GetComponent<AudioSource>().PlayOneShot(menuMusic, .75f);
            //StartCoroutine(menuMusicReturn());
        }

		GetComponent<AudioSource>().PlayOneShot(mainBeat, 1.0f);
    }

    //every .5 seconds
    void HalfBeat()
    {
        GetComponent<AudioSource>().PlayOneShot(halfBeat, .5f);
        StartCoroutine("preBeat");
        x = .75f;
      
        if (dir == 0)
        {
            if (state == 2)
            {
                StartCoroutine(returnState());
                dir = 1;
            }
            StartCoroutine(returnState());
        }
        else if (dir == 1)
        {
            if (state == 0)
            {
                dir = 0;
            }
            StartCoroutine(returnState());
        }
    }

    //every .25 seconds
    void QuarterBeat()
    {
		GetComponent<AudioSource>().PlayOneShot(quarterBeat, .25f);
    }
    
    public IEnumerator menuMusicReturn()
    {
        menu = false;
        yield return new WaitForSeconds(31.5f);
        menu = true;
    }
    
    IEnumerator returnState()
    {
        state = 1;
        yield return new WaitForSeconds(timeInterval / 8);
        state = 0;
    }

    IEnumerator preBeat()
    {
        yield return new WaitForSeconds((timeInterval * 3) / 8);
        state = 2;
    }

    public IEnumerator TogglePause()
    {
        if (paused)
        {
            Time.timeScale = 1;
            //playerMove.enabled = true;
            yield return new WaitForSeconds(.1f);
            paused = false;
        }
        else
        {
            Time.timeScale = 0;
            //playerMove.enabled = false;
            GetComponent<AudioSource>().Stop();
            paused = true;
            yield return new WaitForSeconds(.1f);
        }
    }

    public IEnumerator PauseEnd()
    {
        Time.timeScale = 1;
        yield return new WaitForSeconds(.05f);
        if (paused)
        {
            //playerMove.enabled = true;
            paused = false;
        }
        Application.LoadLevel(0);
    }
}