﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	private float birthTime;

	void Awake () {
		birthTime = Time.time;
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		//Debug.Log(((Time.time - birthTime)+1));
		this.transform.Rotate(Vector3.right * ((Time.time - birthTime)+0.1f) * 50 * Time.deltaTime);
		this.transform.Rotate(Vector3.up * ((Time.time - birthTime)+0.1f) * 50 * Time.deltaTime);
		this.transform.Rotate(Vector3.forward * ((Time.time - birthTime)+0.1f) * 50 * Time.deltaTime);
	}
}
