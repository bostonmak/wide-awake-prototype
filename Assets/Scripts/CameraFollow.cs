﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    public GameObject objectToFollow;
    Camera camera;
    public float distanceBehindObject = 1.0f;
    public float distanceAboveObject = 1.0f;
	public float distanceToRightOfObject = 1.0f;
	public float lerpSpeed = .999999f;
	private bool doLerp = false;

	// Use this for initialization
	void Start () {
        camera = GetComponent<Camera>();
        if (!camera) {
            Debug.LogError("Camera not attached to script");
        }
        transform.position = objectToFollow.transform.position + objectToFollow.transform.up * distanceAboveObject - objectToFollow.transform.forward * distanceBehindObject + objectToFollow.transform.right * distanceToRightOfObject;
		transform.LookAt (objectToFollow.transform.position); //+ objectToFollow.transform.forward);
	}
	
	// Update is called once per frame
    void Update() {
		//transform.position = objectToFollow.transform.position + objectToFollow.transform.up * distanceAboveObject - objectToFollow.transform.forward * distanceBehindObject + objectToFollow.transform.right * distanceToRightOfObject;
		//transform.LookAt (objectToFollow.transform.position); //+ objectToFollow.transform.forward);
		if (doLerp)
			lerp ();

	}

	public void lerp() 
	{
		Debug.Log ("Lerping");
		doLerp = true;
		Vector3 target = objectToFollow.transform.position + objectToFollow.transform.up * distanceAboveObject - objectToFollow.transform.forward * distanceBehindObject + objectToFollow.transform.right * distanceToRightOfObject;
		transform.position = -(target - transform.position)*lerpSpeed + target;
		transform.LookAt (objectToFollow.transform.position);
		if (Vector3.Distance (target, transform.position) < .02)
			doLerp = false;
	}
}
