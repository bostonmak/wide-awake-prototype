﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour 
{
	Transform playerTransform; 
	Quaternion targetLook;


	Vector3 targetMove; 
	Vector3 cameraVelocity;
	public float smoothMove = 0.5f;
	public float cameraSmooth = 0.5f; 
	public float cameraDistance = 5; 
	public float cameraHeight = 3; 
	public float smoothtime = .2f;


	//raycasting camera stuff 
	Vector3 targetMoveUse; 
	public float lerpRaycastSmoothing = 0.1f;


	void Start () 
	{
	playerTransform = GameObject.FindWithTag("Player").transform;
	}

	void Update () 
	{
		targetMove = playerTransform.position + (playerTransform.rotation * new Vector3 (0, cameraHeight,-cameraDistance));

		//raycast stuff
		RaycastHit hit;


		if (Physics.Raycast (playerTransform.position, targetMove - playerTransform.position, out hit, Vector3.Distance(playerTransform.position, targetMove)))
		{ 
			targetMoveUse = Vector3.Lerp (hit.point, playerTransform.position, lerpRaycastSmoothing);
		} 
		else 
		{
			targetMoveUse = targetMove; 
		}
		/*raycast stuff */ 

		//transform.position = Vector3.Lerp (transform.position, targetMove, smoothMove *Time.deltaTime);
		//delete Use from Target Move for no raycast
		transform.position = Vector3.SmoothDamp (transform.position, targetMoveUse, ref cameraVelocity, smoothtime);


		targetLook = Quaternion.LookRotation (playerTransform.position - transform.position);
		transform.rotation = Quaternion.Lerp (transform.rotation, targetLook, cameraSmooth *Time.deltaTime);
	//transform.LookAt (playerTransform);
	}
}
