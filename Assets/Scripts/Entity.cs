﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour 
{

    public float health = 100;
    public float speed = 2.0f;
	protected float curHealth;
	public ParticleSystem death;
    public Animator anim;

	public int damageDealt = 2; 

	// Use this for initialization
	void Start () 
	{
		curHealth = health;
		GameObject.Find ("Managers").GetComponent<PlayerManager>().registerEntity(this);
		if (! death)
			Debug.LogError ("No death particles; continuing anyway!");
		death.Pause();
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (death.isStopped)
		{
			gameObject.SetActive(false);
			GameObject.Find ("Managers").GetComponent<PlayerManager>().removeEntity(this);
		}
        if (anim) {
            anim.SetFloat("Health", health);
        }
	}

	public void reset()
	{
		curHealth = health;
		gameObject.SetActive(true);
	}

    public void ReceiveDamage(float damage) 
	{
        Debug.Log(name + " receiving " + damage + " damage");
        curHealth -= damage;
        if (curHealth <= 0.0f) 
		{
			death.Play ();
			Debug.Log (name + " has died!");
        }
    }
	void OnCollisionStay (Collision col){
		if (col.gameObject.name == "Player"){
		DealingDamage();
		Debug.Log ("Collision is happening");
		}
	}
	void DealingDamage (){
		Player.health -= damageDealt;
		Debug.Log ("Player Health" +Player.health);
	}

}
