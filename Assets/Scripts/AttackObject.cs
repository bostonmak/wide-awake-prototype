﻿using UnityEngine;
using System.Collections;

public class AttackObject : MonoBehaviour {

    public float duration;
    public float damage;
	public Entity owner;
    ArrayList attackedObjects;
	// Use this for initialization
	void Start () {
        damage = 25.0f;
        duration = 0.3f;
        attackedObjects = new ArrayList();
	}
	
	// Update is called once per frame
	void Update () {
        if (duration <= 0.0f) {
            gameObject.SetActive(false);
        }
        duration -= Time.deltaTime;
	}

    public void Activate(bool onBeat) 
	{
		damage = 1.0f;
		if(duration <= 0.0f)//only activate every duration times
		{
	        gameObject.SetActive(true);
	        duration = 0.3f;
	        attackedObjects.Clear();
		}
		if(onBeat)
		{
			GameObject.Find ("Managers").GetComponent<CameraManager>().shakeCamera();
			GameObject.Find ("Managers").GetComponent<Beat>().playSuccess();
			damage = 100.0f;
		}
    }

    void OnTriggerEnter(Collider c) {
        Entity entity = c.GetComponent<Entity>();
        if (entity) {
			Debug.Log ("Entity collidied: " + entity.name);
            if (!entity.Equals(owner) && !attackedObjects.Contains(entity)) {
                entity.ReceiveDamage(damage);
                attackedObjects.Add(entity);
				Debug.Log(entity.name + " attacked!");
            }
        }
    }
}
