﻿using UnityEngine;
using System.Collections;

public class Player : Entity {

	public static float health; 

    public AttackObject attackObject;
	public bool onBeat = false;
    public bool isGrounded = true;
    public float jumpSpeed = 3.0f;
    public float fallSpeed = 5.0f;
    public float rotationSpeed = 125.0f;

    public bool isWalking, isJumping, isAttacking;

	// Use this for initialization
	void Start () {
        health = 100;
        isWalking = false;
        isJumping = false;
        isAttacking = false;
	}
	
	// Update is called once per frame
	void Update () {
        anim.SetBool("IsWalking", isWalking);
        anim.SetBool("IsJumping", isJumping);
        anim.SetBool("IsAttacking", isAttacking);

        AnimatorStateInfo info = anim.GetCurrentAnimatorStateInfo(0);
        if (info.normalizedTime >= 1) {
            if (info.IsName("Jumping")) {
                if (!isGrounded) {
                    isJumping = true;
                }
                else {
                    isJumping = false;
                }
            }
            if (info.IsName("Attacking")) {
                isAttacking = false;
            }
        }
	}

    public void Attack() {
        attackObject.Activate(onBeat);
    }
    void OnCollisionEnter(Collision coll) {
        GameObject collidedWith = coll.gameObject;
        Debug.Log("Collided with: " + collidedWith.name);
        if (coll.gameObject.tag == "Ground") {
            isGrounded = true;
            anim.SetBool("IsJumping", false);
        }
    }
}
