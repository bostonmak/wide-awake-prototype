﻿using UnityEngine;
using System.Collections;

public class CameraManager : MonoBehaviour {

    public static CameraManager mUImger;
    public Camera mMainCamera;
    public PerlinShake perlinShakeComponent;
    public Camera mainCamera {
        get {
            if(mMainCamera == null)
                mMainCamera = Camera.main;
            return mMainCamera;
        }
    }
    public static CameraManager mgr {
        get {
            if(mUImger != null) { return mUImger; }
            GameObject go = GameObject.Find("Managers") as GameObject;
            if(go != null)
            {
                return go.GetComponent<CameraManager>();
            }
            return null;
        }
    }
    float elapsed = 0.0f;
    void Update() {

        //if(Input.GetKeyDown(KeyCode.Space))
        //{
        //    CameraManager.mgr.shakeCamera();
        //}
    }

    float duration = 10;
    public float magnitude = 0.001f;
    public void shakeCamera() {
        perlinShakeComponent.PlayShake();
    }
    
	void GameStart(){
		Debug.Log ("game start");
		Debug.LogError ("need implement game start code here");
	}
}
