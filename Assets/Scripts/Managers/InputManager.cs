﻿using UnityEngine;
using System.Collections;

public class InputManager : MonoBehaviour {

    public bool usingController = false;
    public ParticleSystem shotgunBlast;
    public CameraManager cameraManager;
    public Player player;

	//jumpstuff
	private bool isGrounded = true;

	// Use this for initialization
	void Start () {
        // Detect Controller presence
//         if (Input.GetJoystickNames().Length > 0) {
//             usingController = true;
//         }
//         else {
//             usingController = false;
		cameraManager = CameraManager.mgr;
		if (player == null)
			Debug.LogError("No player object to manage!");
		else
			Debug.Log ("Player is: " + player.name);
        if (Input.GetJoystickNames().Length > 0) {
            usingController = true;
        }
        else {
            usingController = false;
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (!usingController) {
            //wasd controls
            Vector3 newPosition = player.GetComponent<Transform>().position;
            if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D)) {
                //Debug.Log("stop");
                player.isWalking = false;
            }
            if (Input.GetKey(KeyCode.W)) {
				newPosition += player.transform.forward * player.speed * Time.deltaTime;
                player.isWalking = true;
				cameraManager.mainCamera.GetComponent<CameraFollow>().lerp ();
				if (Input.GetKeyDown(KeyCode.A))
				{
					player.transform.Rotate(Vector3.up, -45, Space.Self);
				}
				if (Input.GetKeyDown(KeyCode.D))
				{
					player.transform.Rotate(Vector3.up, 45, Space.Self);
				}
				if (Input.GetKeyUp(KeyCode.A)) {
					player.transform.Rotate(Vector3.up, 45, Space.Self);
					//newPosition -= player.transform.forward * speed * Time.deltaTime;
				}
				if (Input.GetKeyUp(KeyCode.D)) {
					player.transform.Rotate(Vector3.up, -45, Space.Self);
					//newPosition -= player.transform.forward * speed * Time.deltaTime;
				}
			}
            
			else//not moving forward
			{
				cameraManager.mainCamera.GetComponent<CameraFollow>().lerp ();

				if (Input.GetKeyDown(KeyCode.D)) {
					player.GetComponent<Transform>().Rotate(Vector3.up, 90, Space.Self);
				}
				if (Input.GetKey(KeyCode.D)) {
					newPosition += player.GetComponent<Transform>().forward * player.speed * Time.deltaTime;
					player.isWalking = true;
				}
				if (Input.GetKeyUp(KeyCode.D)) {
					cameraManager.mainCamera.GetComponent<CameraFollow>().lerp ();
				}

				if (Input.GetKeyDown(KeyCode.A)) {
					player.GetComponent<Transform>().Rotate(Vector3.up, -90, Space.Self);
				}
				if (Input.GetKey(KeyCode.A)) {
					newPosition += player.GetComponent<Transform>().forward * player.speed * Time.deltaTime;
					player.isWalking = true;
				}
				if (Input.GetKeyUp(KeyCode.A)) {
					cameraManager.mainCamera.GetComponent<CameraFollow>().lerp ();
				}

	            if (Input.GetKeyDown(KeyCode.S)) {
					//play reverse animation
					player.GetComponent<Transform>().Rotate(Vector3.up, -180, Space.Self);
					cameraManager.mainCamera.GetComponent<CameraFollow>().lerp ();
		
				}
				if (Input.GetKey(KeyCode.S)) {
					newPosition += player.GetComponent<Transform>().forward * player.speed * Time.deltaTime;
					player.isWalking = true;
				}
				/**
	            if (Input.GetKey(KeyCode.LeftArrow)) {
					player.GetComponent<Transform>().Rotate(Vector3.up, -rotationSpeed * Time.deltaTime, Space.Self);
	            }
	            if (Input.GetKey(KeyCode.RightArrow)) {
					player.GetComponent<Transform>().Rotate(Vector3.up, rotationSpeed * Time.deltaTime, Space.Self);
	            }
	            **/
			}
            if (Input.GetKey(KeyCode.Space)) {
                if (shotgunBlast) {
                    //cameraManager.shakeCamera();
                    player.Attack();
                    shotgunBlast.Emit(1);
                    player.isAttacking = true;
                }
            }

			if (Input.GetKey(KeyCode.RightShift) && player.isGrounded){
				player.GetComponent<Rigidbody>().AddForce(Vector3.up * player.jumpSpeed * Time.deltaTime);
                //newPosition += transform.up * player.jumpSpeed * Time.deltaTime;
                player.isJumping = true;
				isGrounded = false;
			}

            //transform.LookAt(newPosition);
			player.GetComponent<Transform>().position = newPosition;
        }
        else {//code broken atm
            // use xbox controller (windows only)
            Vector3 newPosition = transform.position;
            Vector3 joystick1 = new Vector3(Input.GetAxis("LeftJoystickX"), 0.0f, Input.GetAxis("LeftJoystickY"));
            Vector3 joystick2 = new Vector3(Input.GetAxis("RightJoystickX"), 0.0f, 0.0f); // dont need vertical input for camera atm.
            //Debug.Log(joystick2);
            // flicking joystick causes rotation, checking magnitude will prevent this
            if (joystick1.magnitude >= 0.1f) {
                newPosition += (transform.forward * joystick1.z + transform.right * joystick1.x) * player.speed * Time.deltaTime;
                transform.position = newPosition;
                player.isWalking = true;
            }
            else {
                player.isWalking = false;
            }
            if (joystick2.magnitude >= 0.1f) {
                player.transform.Rotate(Vector3.up, joystick2.x * player.rotationSpeed * Time.deltaTime, Space.Self);
        
            }

            if (Input.GetButtonDown("ButtonA")) {
                // do particle effect stuff here
                //Debug.Log("ButtonA Press");
                if (shotgunBlast) {
                    cameraManager.shakeCamera();
                    player.Attack();
                    shotgunBlast.Emit(1);
                    player.isAttacking = true;
                }
			if (Input.GetButtonDown("ButtonB") && player.isGrounded){
                newPosition += player.transform.up * player.jumpSpeed * Time.deltaTime;
                player.isGrounded = false;
                player.isJumping = true;
				}
            }
        }
	}
		void OnCollisionEnter (Collision coll){
		GameObject collidedWith = coll.gameObject;
		Debug.Log("Collided with: " + collidedWith.name);
		if(coll.gameObject.tag == "Ground"){
					isGrounded = true;
				}
			}
}
				
