﻿using UnityEngine;
using System.Collections;

public class PlayerManager : MonoBehaviour {

	private ArrayList entities = new ArrayList();
	private ArrayList deadEnts = new ArrayList();
	private GameObject title;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(entities.Count == 0)
			gameEnd();
	
	}

	public void registerEntity(Entity e)
	{
		entities.Add (e);
	}

	public void removeEntity(Entity e)
	{
		deadEnts.Add(e);
		entities.Remove (e);
	}

	//received from login screen
	void gameStart() 
	{
		//currently, just destroys canvas
		title = GameObject.Find ("Canvas");
		title.SetActive(false);

	}

	void gameEnd()
	{
		Debug.Log ("Game ending");
		foreach(Entity e in deadEnts)
		{
			e.reset();
			entities.Add (e);
		}
		deadEnts = new ArrayList();
		title.SetActive (true);
	}
}
