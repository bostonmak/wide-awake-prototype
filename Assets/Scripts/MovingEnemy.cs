﻿using UnityEngine;
using System.Collections;

public class MovingEnemy : Entity 
{

	public Transform point1;
	public Transform point2;
	private int dest = 2;

	// Use this for initialization
	void Start () 
	{
		NavMeshAgent agent = GetComponent<NavMeshAgent>();
		if (!point1 || !point2) 
		{
			Debug.LogError("Patrol Points not set!");
		}
		if (!agent) 
		{
			Debug.LogError("NavAgent not attached to script");
		}
		agent.destination = point2.position; 
	}
	
	// Update is called once per frame
	void Update () 
	{
		NavMeshAgent agent = GetComponent<NavMeshAgent>();
		Vector3 newPosition = transform.position;
		if (Vector3.Distance(newPosition, agent.destination) < 2)
		{
			if (dest == 1)
			{
				agent.destination = point2.position;
				dest = 2;
			}
			else
			{
				agent.destination = point1.position;
				dest = 1;
			}
		}	
	
	}
}
