﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenGlow : MonoBehaviour {
	public UnityEngine.UI.RawImage sp;
	float dist,fadt,timer,deltaAlpha,currAlpha;
	Color mycolor;
	// Use this for initialization
	void Start () {

	}
	public bool test;
	// Update is called once per frame
	void Update () {
		sp.rectTransform.localScale = new Vector3 (Screen.width, Screen.height, 0);
		if (timer <= 0) {
			gameObject.SetActive(false);
		} else {
			if(dist>0){
				dist -= Time.deltaTime;
			}else if(fadt>0){
				fadt -=Time.deltaTime;
				sp.color = new Color(mycolor.r,mycolor.g,mycolor.b,currAlpha);
				currAlpha -= Time.deltaTime * deltaAlpha;
			}else{
				timer = -1;
			}
		}
		timer -= Time.deltaTime;
		
		
		if (test) {
			test = false;
			glow(new Color(1,0,0,0),1,1);
		}
	}

	public void glow(Color glowcolor, float displayTime, float fadingtime){
		if (displayTime <= 0)
			return;
		dist = displayTime;
		fadt = fadingtime;
		timer = displayTime + fadingtime;
		gameObject.SetActive (true);
		sp.color = glowcolor;
		mycolor = glowcolor;
		currAlpha = glowcolor.a;
		if (fadingtime > 0) {
			deltaAlpha = 1/fadt;
		}
	}
}
